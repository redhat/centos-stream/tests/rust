// From nodejs doc https://nodejs.org/api/wasi.html

'use strict';
const { readFile } = require('fs/promises');
const { WASI } = require('wasi');
const { argv, env } = require('process');
const { join } = require('path');

const wasi = new WASI({
  args: ["demo.wasm", "/sandbox/input.txt", "/sandbox/output.txt"],
  env,
  version: 'preview1',
});

const importObject = { wasi_snapshot_preview1: wasi.wasiImport };
(async () => {
    const wasm = await WebAssembly.compile(
        await readFile(join(__dirname, 'demo.wasm'))
    );
    const instance = await WebAssembly.instantiate(wasm, importObject);

    wasi.start(instance);
})();
