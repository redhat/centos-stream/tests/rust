#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES="rust cargo"
if rlIsFedora; then
    PACKAGES+=" cargo-rpm-macros"
else
    PACKAGES+=" rust-toolset"
fi
SPECFILE="rust-rpmtest.spec"
PKGNAME="$(rpmspec -q --srpm --qf "%{name}-%{version}-%{release}.%{arch}" $SPECFILE)"
CRATEVER="$(rpmspec -q --srpm --qf "%{version}" $SPECFILE)"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "mkdir -p $tmp/{BUILD,BUILDROOT,SPECS,SOURCES}"

        rlRun "tar czf rpmtest.tar.gz rpmtest --transform s/rpmtest/rpmtest-${CRATEVER}/"
        rlRun "mv rpmtest.tar.gz $tmp/SOURCES"

        # Vendor and package vendored sources
        rlRun "pushd rpmtest"
        rlRun "cargo vendor"
        rlRun "tar czf rpmtest-vendor.tar.gz vendor"
        rlRun "rm -rf vendor"
        rlRun "mv rpmtest-vendor.tar.gz $tmp/SOURCES"
        rlRun "popd"

        # Set the spec file
        rlRun "cp $SPECFILE $tmp/SPECS"
        rlRun "pushd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "rpmbuild --define '_topdir $tmp' -ba $tmp/SPECS/$SPECFILE"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
