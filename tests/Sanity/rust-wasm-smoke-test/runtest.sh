#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/rust/Sanity/rust-wasm-smoke-test
#   Description: Test that the rust wasm target is enabled and can compile correctly
#   Author: Jesus Checa <jcheca@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES="$(rpm -qf "$(which rustc)") rust-std-static-wasm32-unknown-unknown"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        if rlIsRHEL 8 9; then
            # In RHEL 8 and 9 nodejs is shipped in different versions using modules.
            # We always want to use the latest version.
            rlRun "NODE_VER=\"$(dnf module info nodejs | sed -n -E 's|^Stream\s+:\s*([0-9]+).*|\1|p' | sort | tail -1)\""
            rlRun "yum -y module switch-to nodejs:$NODE_VER"
            rlRun "yum -y module install nodejs"
        fi
        rlRun "cp lib.rs $TmpDir"
        rlRun "cp test.js $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "rustc --target wasm32-unknown-unknown --crate-type=cdylib lib.rs -o fib.wasm" 0 "Building WASM binary"
        rlRun "node test.js" 0 "Testing WASM binary"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalEnd
