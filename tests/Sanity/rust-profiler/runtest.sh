#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="$(rpm -qf $(which rustc))"
PACKAGES=${PACKAGES:-$PACKAGE}

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "cp main.rs coverage.exp $tmp"
        rlRun "pushd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

    rlPhaseStartTest "Test instrumentation coverage"
        rlRun "rustc -C instrument-coverage main.rs -o main"
        rlRun "LLVM_PROFILE_FILE='testprof.profraw' ./main"
        # Convert and inspect the profile data
        rlRun "llvm-profdata merge -sparse testprof.profraw -o testprof.profdata"
        rlRun "llvm-cov show  main -instr-profile=testprof.profdata > coverage.out"
        # Remove the trailing newline from dumping llvm-cov output into the file.
        rlRun "truncate -s -1 coverage.out"
        # Check coverage reports as expected
        rlRun "diff coverage.exp coverage.out"
    rlPhaseEnd

    rlPhaseStartTest "Test PGO build"
        rlRun "rustc -C profile-generate main.rs -o main"
        # Run the compiled binary to generate the profiling data
        rlRun "LLVM_PROFILE_FILE='testprof.profraw' ./main"
        # Convert and inspect the profile data
        rlRun "llvm-profdata merge -sparse testprof.profraw -o testprof.profdata"

        # Now rebuild with the gathered profiling data
        rlRun "rustc -C profile-use=testprof.profdata main.rs -o main_pgo"
        rlRun "./main_pgo"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
