// Credit where credit is due. This test was made following this article:
// https://vulns.xyz/2023/03/linux-executable-from-scratch-with-x86_64-unknown-none-rust/

// Opt-out the use of std, as x86_64-unknown-none doesn't have standard library
#![no_std]
// Inform the compiler that we don't have a main function, we'll be specifying
// the program entry point using global_asm.
#![no_main]

use core::arch::asm;
use core::arch::global_asm;
use core::panic::PanicInfo;

// As we don't have std library, we need to write our own panic handler
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    exit(1);
}

// Program entry point, adjust stack pointer and call main
global_asm! {
    ".global _start",
    "_start:",
    "mov rdi, rsp",
    "call main"
}

// exit syscall
fn exit(status: i32) -> ! {
    unsafe {
        asm!(
            "syscall",
            in("rax") 60,
            in("rdi") status,
            options(noreturn)
        );
    }
}

// write syscall
unsafe fn write(fd: i32, buf: *const u8, count: usize) -> isize {
    let r0;
    asm!(
        "syscall",
        inlateout("rax") 1isize => r0,
        in("rdi") fd,
        in("rsi") buf,
        in("rdx") count,
        lateout("rcx") _,
        lateout("r11") _,
        options(nostack, preserves_flags)
    );
    r0
}

// Tell the compiler to not mangle the function name. We're calling main
// with this name from global_asm
#[no_mangle]
unsafe fn main(_stack_top: *const u8) -> ! {
    write(1, b"Hello world\n".as_ptr(), 12);
    exit(0);
}
