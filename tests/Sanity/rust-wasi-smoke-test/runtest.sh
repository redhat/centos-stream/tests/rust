#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/rust/Sanity/rust-wasi-smoke-test
#   Description: Compile and run a wasi app from rust
#   Author: Jesus Checa <jcheca@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES="$(rpm -qf "$(which rustc)")"
PACKAGES+=" $(rlGetYAMLdeps)"

# TMT will decide which targets to install depending on the distro/arch context.
# Here we only check which ones were installed.
ALL_TARGETS=(wasm32-wasi wasm32-wasip1 wasm32-wasip2)
TEST_TARGETS=()
for target in "${ALL_TARGETS[@]}"; do
    rpm -q rust-std-static-"$target" && TEST_TARGETS+=("$target")
done

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        if rlIsRHEL 8 9; then
            # In RHEL 8 and 9 nodejs is shipped in different versions using modules.
            # We always want to use the latest version.
            rlRun "NODE_VER=\"$(dnf module info nodejs | sed -n -E 's|^Stream\s+:\s*([0-9]+).*|\1|p' | sort | tail -1)\""
            rlRun "yum -y module switch-to nodejs:$NODE_VER"
            rlRun "yum -y module install nodejs"
        fi
        # Example taken from WASI documentation:
        # https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md#from-rust
        rlRun "cp demo.rs $TmpDir"
        rlRun "cp wasi_test*.js $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for target in "${TEST_TARGETS[@]}"; do
        rlPhaseStartTest "Target $target"
            rlRun "rustc --target $target demo.rs -o demo.wasm" 0 "Building WASI binary"
            rlAssertExists "demo.wasm"

            ### Test sandboxing

            TESTSTR="This is a test string"
            rlRun "echo \"$TESTSTR\" > input.txt"

            # Test without preopening
            rlRun "node --experimental-wasi-unstable-preview1 wasi_test_no_preopen.js 2> node.out"
            rlAssertGrep "error opening" node.out
            rlAssertGrep "failed to find a pre-opened file descriptor" node.out
            rlAssertNotExists "output.txt"

            # Test with pre-opened directory
            rlRun "node --experimental-wasi-unstable-preview1 wasi_test_preopen.js 2> node.out"
            rlAssertNotGrep "error opening" node.out
            rlAssertExists "output.txt"
            rlAssertGrep "$TESTSTR" output.txt

            # Cleanup
            rlRun "rm -rf demo.wasm node.out output.txt"
        rlPhaseEnd
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalEnd
